<?php 
session_start();
include "koneksi.php";

if(!isset($_SESSION['id_user']))
{
    echo "<script>window.location='login.php'</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>KALTIMPOST</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="font-size: 20px;">KALTIMPOST</a> 
            </div>
            <div style="color: white; padding: 15px 50px 5px 50px; float: right;font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				    <li class="text-center">
                        <img src="assets/img/find_user.png" class="user-image img-responsive" width="90" />
                        <h3 style="color:#fff; margin-top:-10px;"><?php echo ucfirst ($_SESSION['login_nama']); ?></h3>
					</li>

                    <li> <a href="index.php"><i class="fa fa-dashboard fa-3x"></i> Home</a> </li>
                    <li> <a href="index.php?halaman=admin"><i class="fa fa-share fa-3x"></i> Berita</a> </li>                    
                    <li> <a href="index.php?halaman=detail"><i class="fa fa-share fa-3x"></i> Detail Berita</a> </li>
					<li> <a href="index.php?halaman=komentar"><i class="fa fa-share fa-3x"></i> Kritik dan Saran</a> </li>
					<li> <a href="index.php?halaman=portal"><i class="fa fa-share fa-3x"></i> Portal Berita</a> </li>
					<li> <a href="index.php?halaman=detailportal"><i class="fa fa-share fa-3x"></i>Detail Portal Berita</a> </li>
                    
                </ul>
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <?php
                    if (isset($_GET['halaman'])) 
                    {
                        if ($_GET['halaman']=="admin")
                        {
                            include "admin/admin.php";
                        }
                        elseif ($_GET['halaman']=="tambah")
                        {
                            include "admin/tambah.php";
                        }
                        elseif ($_GET['halaman']=="edit")
                        {
                            include "admin/edit.php";
                        }
                        elseif ($_GET['halaman']=="delete")
                        {
                            include "admin/delete.php";
                        }

                        elseif ($_GET['halaman']=="detail")
                        {
                            include "detail/detail.php";
                        }
                        elseif ($_GET['halaman']=="tambah_berita")
                        {
                            include "detail/tambah_berita.php";
                        }
                        elseif ($_GET['halaman']=="edit_berita")
                        {
                            include "detail/edit_berita.php";
                        }
                        elseif ($_GET['halaman']=="delete_berita")
                        {
                            include "detail/delete_berita.php";
                        }
						elseif ($_GET['halaman']=="komentar")
                        {
                            include "komentar/komentar.php";
                        }
                        elseif ($_GET['halaman']=="tambah_komentar")
                        {
                            include "komentar/tambah_komentar.php";
                        }
                        elseif ($_GET['halaman']=="edit_komentar")
                        {
                            include "komentar/edit_komentar.php";
                        }
                        elseif ($_GET['halaman']=="delete_komentar")
                        {
                            include "komentar/delete_komentar.php";
                        }
						elseif ($_GET['halaman']=="portal")
                        {
                            include "portal/portal.php";
                        }
                        elseif ($_GET['halaman']=="tambah_portal")
                        {
                            include "portal/tambah_portal.php";
                        }
                        elseif ($_GET['halaman']=="edit_portal")
                        {
                            include "portal/edit_portal.php";
                        }
                        elseif ($_GET['halaman']=="delete_portal")
                        {
                            include "portal/delete_portal.php";
                        }
						elseif ($_GET['halaman']=="detailportal")
                        {
                            include "detailportal/detailportal.php";
                        }
                        elseif ($_GET['halaman']=="tambah_detailportal")
                        {
                            include "detailportal/tambah_detailportal.php";
                        }
                        elseif ($_GET['halaman']=="edit_detailportal")
                        {
                            include "detailportal/edit_detailportal.php";
                        }
                        elseif ($_GET['halaman']=="delete_detailportal")
                        {
                            include "detailportal/delete_detailportal.php";
                        }
                    }
                    else 
                    {
                        include "home.php";
                    }
                ?>
            </div> <!-- /. PAGE INNER  -->
        </div> <!-- /. PAGE WRAPPER  -->
    </div> <!-- /. WRAPPER  -->

    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>