<h2 class="text-center">DETAIL PORTAL BERITA </h2>
    <a href="index.php?halaman=tambah_detailportal" class="btn btn-primary">Tambah Data</a>
    <br><br>
<table class="table table-bordered table-striped">
    <tr>
        <th>No</th>
        <th>ID</th>
        <th>Judul Berita</th>
        <th>Isi Berita</th>
        <th>Image</th>
		<th>Aksi</th>
    </tr>
    <?php
$no=1;
$hasil=$koneksi->query("SELECT * FROM detail_news");
while($row=mysqli_fetch_array($hasil))
{
    $id = $row['id']
?>
        <tr>
            <td>
                <?php echo $no++ ?>
            </td>
            <td>
                <?php echo $row['id']; ?>
            </td>
            <td>
                <?php echo ucfirst($row['judul_berita']); ?>
            </td>
            <td>
                <?php echo ucfirst($row['isi_berita']); ?>
            </td>
            <td>
                <img src="<?php echo ($row['img']); ?>" width="100" height="100">
            </td>
            <td>
                <a href="index.php?halaman=edit_detailportal&id=<?php echo $id ?>" class="btn btn-info">Edit</a>
                <a href="index.php?halaman=delete_detailportal&id=<?php echo $id ?>" onClick="return confirm('Apakah anda yakin akan hapus data ini?')" class="btn btn-danger">Delete</a>
            </td>
        </tr>
<?php } ?>
</table>
