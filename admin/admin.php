<h2 class="text-center">DATA </h2>
    <a href="index.php?halaman=tambah" class="btn btn-primary">Tambah Data</a>
    <br><br>
<table class="table table-bordered table-striped">
    <tr>
        <th>No</th>
        <th>ID</th>
        <th>Tittle</th>
        <th>Keterangan</th>
        <th>Image</th>
		<th>Aksi</th>
    </tr>
    <?php
$no=1;
$hasil=$koneksi->query("SELECT * FROM tbl_image");
while($row=mysqli_fetch_array($hasil))
{
    $id = $row['id']
?>
        <tr>
            <td>
                <?php echo $no++ ?>
            </td>
            <td>
                <?php echo $row['id']; ?>
            </td>
            <td>
                <?php echo ucfirst($row['tittle']); ?>
            </td>
            <td>
                <?php echo ucfirst($row['keterangan']); ?>
            </td>
            <td>
                <img src="<?php echo ($row['img']); ?>" width="100" height="100">
            </td>
            <td>
                <a href="index.php?halaman=edit&id=<?php echo $id ?>" class="btn btn-info">Edit</a>
                <a href="index.php?halaman=delete&id=<?php echo $id ?>" onClick="return confirm('Apakah anda yakin akan hapus data ini?')" class="btn btn-danger">Delete</a>
            </td>
        </tr>
<?php } ?>
</table>
